from sqlalchemy import create_engine, MetaData
from databases import Database
from passlib.context import CryptContext
from sqlalchemy.orm import Session, sessionmaker
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy import create_engine, Column, Integer, String, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DATABASE_URL = "postgresql://postgres:Nurbolat2004@host.docker.internal:5432/postgres"
database = Database(DATABASE_URL)
engine = create_engine(DATABASE_URL)
metadata = MetaData()  
Base = declarative_base(metadata=metadata)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


class Item(Base):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)


Base.metadata.create_all(bind=engine)


async def get_db():
    try:
        await database.connect()
        yield database
    finally:
        await database.disconnect()


password_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
