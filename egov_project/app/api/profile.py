from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse
from app.db import get_db
from sqlalchemy.orm import Session
from sqlalchemy import select
from app.models import User
from fastapi.templating import Jinja2Templates
from databases.backends.postgres import Record

router = APIRouter()
templates = Jinja2Templates(directory="templates")


async def convert_dict(record: Record):
    return dict(record)


@router.get("/profile/{user_id}", response_class=HTMLResponse)
async def profile_detail(request: Request, user_id: int, db: Session = Depends(get_db)):
    try:
        user_data = await db.fetch_one(select(User).where(User.id == user_id))
        user_data_dict = await convert_dict(user_data)
        context = {"request": request, "user_data": user_data_dict}
        context = {"request": request, "user_data": user_data}
        return templates.TemplateResponse("profile_detail.html", context)
    except Exception as e:
        return {"error": str(e)}