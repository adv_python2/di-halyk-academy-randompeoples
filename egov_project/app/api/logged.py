from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from app.db import get_db
from sqlalchemy.orm import Session
from sqlalchemy import select
from app.models import User
from fastapi.templating import Jinja2Templates
from databases.backends.postgres import Record
from fastapi import Path

router = APIRouter()
templates = Jinja2Templates(directory="templates")


async def convert_record_to_dict(record: Record):
    return dict(record)


@router.get("/logged/{user_id}", response_class=HTMLResponse)
async def login_page(request: Request, user_id: int = Path(...), db: Session = Depends(get_db)):
    try:
        user_data = await db.fetch_one(select(User).where(User.id == user_id))
        context = {
            "request": request,
            "user_data": user_data,
        }
        return templates.TemplateResponse("logged.html", context)

    except Exception as e:
        return {"error": str(e)}


@router.get("/request/{user_id}", response_class=HTMLResponse)
async def request_form(request: Request, user_id: int, db: Session = Depends(get_db)):
    try:
        user_data = await db.fetch_one(select(User).where(User.id == user_id))
        user_data_dict = await convert_record_to_dict(user_data)
        context = {"request": request, "user_data": user_data_dict}
        return templates.TemplateResponse("request.html", context)
    except Exception as e:
        return {"error": str(e)}
