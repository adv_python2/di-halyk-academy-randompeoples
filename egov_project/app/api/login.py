from fastapi import APIRouter, Depends, HTTPException, Form, Request
from fastapi.responses import HTMLResponse
from app.db import get_db, password_context
from sqlalchemy.orm import Session
from sqlalchemy import select
from app.models import User
from fastapi.templating import Jinja2Templates

router = APIRouter()

templates = Jinja2Templates(directory="templates")


@router.get("/", response_class=HTMLResponse)
async def login_form(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})


@router.post("/login")
async def login(
        request: Request,
        iin: str = Form(...),
        password: str = Form(...),
        db: Session = Depends(get_db)
):
    try:
        existing_user = await db.fetch_one(select(User).where(User.iin == iin))

        if existing_user and password_context.verify(password, existing_user['hashed_password']):
            is_manager = existing_user['is_manager']

            if is_manager:
                return templates.TemplateResponse("manager.html", {"request": request, "user_data": existing_user})
            else:
                return templates.TemplateResponse("logged.html", {"request": request, "user_data": existing_user})
        else:
            raise HTTPException(status_code=401, detail="Invalid credentials")

    except Exception as e:
        return templates.TemplateResponse("error.html", {"request": request, "error_message": str(e)})
