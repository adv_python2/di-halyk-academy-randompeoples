from fastapi import APIRouter, Depends, HTTPException, Form, Request
from fastapi.responses import HTMLResponse
from app.db import get_db, password_context
from sqlalchemy.orm import Session
from sqlalchemy import select
from datetime import datetime
from app.models import User
from fastapi.templating import Jinja2Templates

router = APIRouter()
templates = Jinja2Templates(directory="templates")


@router.get("/register", response_class=HTMLResponse)
async def register_form(request: Request):
    return templates.TemplateResponse("register.html", {"request": request})


@router.post("/register")
async def register(
        request: Request,
        iin: str = Form(...),
        name: str = Form(...),
        sur_name: str = Form(...),
        phone_number: str = Form(...),
        gender: str = Form(None),
        about_me: str = Form(None),
        education: str = Form(None),
        birthdate: str = Form(None),
        password: str = Form(...),
        confirm_password: str = Form(...),
        db: Session = Depends(get_db)
):
    try:
        birthdate = datetime.strptime(birthdate, "%Y-%m-%d").date()

        existing_user_iin = await db.fetch_one(select(User).where(User.iin == iin))
        if existing_user_iin:
            raise Exception("IIN is already registered")

        existing_user_phone = await db.fetch_one(select(User).where(User.phone_number == phone_number))
        if existing_user_phone:
            raise Exception("Phone number is already registered")
        if len(password) < 8:
            raise Exception("Password must be at least 8 characters")
        if password != confirm_password:
            raise Exception("Passwords do not match")
        hashed_password = password_context.hash(password)

        new_user = User(
            iin=iin,
            name=name,
            sur_name=sur_name,
            phone_number=phone_number,
            hashed_password=hashed_password,
            gender=gender,
            about_me=about_me,
            education=education,
            birthdate=birthdate
        )

        await db.execute(User.__table__.insert().values(
            iin=new_user.iin,
            name=new_user.name,
            sur_name=new_user.sur_name,
            phone_number=new_user.phone_number,
            gender=new_user.gender,
            about_me=new_user.about_me,
            education=new_user.education,
            birthdate=new_user.birthdate,
            hashed_password=new_user.hashed_password
        ))

        return templates.TemplateResponse("registered.html", {"request": request, "user_data": new_user})

    except Exception as e:
        return templates.TemplateResponse("error.html", {"request": request, "error_message": str(e)})
