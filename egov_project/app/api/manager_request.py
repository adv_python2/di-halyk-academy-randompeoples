from datetime import datetime
from fastapi import APIRouter, Depends, Request, HTTPException, status
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from app.db import get_db
from sqlalchemy import select, update
from databases.backends.postgres import Record
from app.models import ProfileChangeRequest, User
from fastapi.responses import JSONResponse
import json

manager_router = APIRouter()
templates = Jinja2Templates(directory="templates")


async def convert_record_to_dict(record: Record):
    return dict(record)


@manager_router.get("/manager/profile_change_requests", response_class=HTMLResponse)
async def view_profile_change_requests(request: Request, db=Depends(get_db)):
    try:
        profile_change_requests = await db.fetch_all(select(ProfileChangeRequest).where(ProfileChangeRequest.status == "pending"))
        print(profile_change_requests)
        json_profile_change_requests = []
        for profile_change_request in profile_change_requests:
            json_profile_change_requests.append(dict(profile_change_request))
        print(json_profile_change_requests)
        context = {"request": request, "profile_change_requests": json_profile_change_requests}
        return templates.TemplateResponse("manager_profile_change_requests.html", context)
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=500)


@manager_router.post("/manager/profile_change_requests/{request_id}/approve")
async def approve_request(request: Request, request_id: int, db=Depends(get_db)):
    print(request_id)
    profile_change_request = await db.fetch_one(
        select(ProfileChangeRequest).where(ProfileChangeRequest.id == request_id)
    )
    if profile_change_request is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ProfileChangeRequest with id {request_id} not found",
        )
    new_data = profile_change_request['new_data']
    print(type(new_data))
    new_data = json.loads(new_data)
    print(new_data)
    birthdate = datetime.strptime(new_data['birthdate'], "%Y-%m-%d").date()

    answer = await db.execute(update(User).where(User.id == profile_change_request.user_id).values(
        name=new_data['name'],
        sur_name=new_data['sur_name'],
        phone_number=new_data['phone_number'],
        about_me=new_data['about_me'],
        education=new_data['education'],
        birthdate=birthdate,
    ))
    print(answer)
    update_statement = (
        update(ProfileChangeRequest)
        .where(ProfileChangeRequest.id == request_id)
        .values(status="approved")
        .returning(ProfileChangeRequest)
    )

    updated_request = await db.execute(update_statement)
    print(updated_request)
    if updated_request is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ProfileChangeRequest with id {request_id} not found",
        )

    return templates.TemplateResponse("request_approved.html", {"request": request, "user_data": new_data})


@manager_router.post("/manager/profile_change_requests/{request_id}/reject")
async def reject_request(request: Request, request_id: int, db=Depends(get_db)):
    update_statement = (
        update(ProfileChangeRequest)
        .where(ProfileChangeRequest.id == request_id)
        .values(status="rejected")
        .returning(ProfileChangeRequest)
    )

    updated_request = await db.execute(update_statement)
    print(updated_request)
    profile_change_request = await db.fetch_one(
        select(ProfileChangeRequest).where(ProfileChangeRequest.id == request_id)
    )
    if updated_request is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ProfileChangeRequest with id {request_id} not found",
        )
    new_data = profile_change_request['new_data']
    new_data = json.loads(new_data)
    return templates.TemplateResponse("request_rejected.html",{"request": request, "user_data": new_data})
