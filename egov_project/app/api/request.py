from fastapi import APIRouter, Depends, Request, status, Query
from fastapi.responses import HTMLResponse, RedirectResponse
from app.db import get_db, SessionLocal
from sqlalchemy.orm import Session
from sqlalchemy import select
from app.models import User
from fastapi.templating import Jinja2Templates
from app.models import ProfileChangeRequest
import json
import httpx
from app.models import Company
from fastapi.responses import RedirectResponse
from fastapi import Depends
from fastapi import Form
from typing import Dict
from sqlalchemy import func
from datetime import datetime, timedelta
from sqlalchemy import cast, JSON, and_
from fastapi import APIRouter, Depends, HTTPException, Form, Request
from app.stat_integration.stat_api import (
    get_company_data_by_bin_1,
    get_company_data_by_bin_2,
    get_company_data_by_params,
    check_request_status,
    download_request_data,
)

router = APIRouter()
templates = Jinja2Templates(directory="templates")


async def convert_record_to_dict(record):
    return dict(record)


@router.get("/profile/{user_id}/edit", response_class=HTMLResponse)
async def edit_profile_form(request: Request, user_id: int, db: Session = Depends(get_db)):
    try:
        user_data = await db.fetch_one(select(User).where(User.id == user_id))
        user_data_dict = await convert_record_to_dict(user_data)
        context = {"request": request, "user_data": user_data_dict}
        return templates.TemplateResponse("edit_profile.html", context)
    except Exception as e:
        return {"error": str(e)}


@router.post("/profile/{user_id}/edit")
async def edit_profile(
        request: Request,
        user_id: int,
        profile_change: str = Form(...),
        name: str = Form(...),
        sur_name: str = Form(...),
        phone_number: str = Form(...),
        about_me: str = Form(...),
        education: str = Form(...),
        birthdate: str = Form(...),
        db: Session = Depends(get_db),
):
    if profile_change == 'true':
        new_profile_data = {
            'name': name,
            'sur_name': sur_name,
            'phone_number': phone_number,
            'about_me': about_me,
            'education': education,
            'birthdate': str(birthdate),
        }

        current_user = await db.fetch_one(select(User).where(User.id == user_id))
        old_profile_data = dict()
        for key in new_profile_data.keys():
            old_profile_data[key] = getattr(current_user, key)

        old_profile_data['birthdate'] = str(old_profile_data['birthdate'])
        if old_profile_data!=new_profile_data:
            change_request = ProfileChangeRequest(
                old_data=json.dumps(old_profile_data),
                new_data=json.dumps(new_profile_data),
                user_id=user_id,
                status='pending'
            )
            await db.execute(
                ProfileChangeRequest.__table__.insert().values(
                    old_data=change_request.old_data,
                    new_data=change_request.new_data,
                    user_id=change_request.user_id,
                    status=change_request.status
                )
            )
            return templates.TemplateResponse("request_submitted.html", {"request": request, "user_data": current_user})
        else:
            return templates.TemplateResponse("error.html", {"request": request, "error_message": "No changes were made"})



@router.get("/profile/{user_id}/stat", response_class=HTMLResponse)
async def stat_integration_form(request: Request, user_id: int, db: Session = Depends(get_db)):
    user_data = await db.fetch_one(select(User).where(User.id == user_id))
    user_data_dict = dict(user_data)

    context = {"request": request, "user_data": user_data_dict}
    return templates.TemplateResponse("stat_integration_form.html", context)

from sqlalchemy.dialects.postgresql import JSONB

@router.post("/profile/{user_id}/stat")
async def stat_integration(
        request: Request,
        bin_number: int = Form(...),
        db: Session = Depends(get_db)
):
    try:
        company_data_1 = await get_company_data_by_bin_1(bin_number)
        company_data_2 = await get_company_data_by_bin_2(bin_number)

        if not company_data_1 or not isinstance(company_data_1, dict):
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Invalid response from Stat API")

        if not company_data_2 or not isinstance(company_data_2, dict):
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Invalid response from Stat API")

        Company_request = Company(
            bin=str(bin_number),  # Привести к строке
            company_data_1=company_data_1,
            company_data_2=company_data_2,
            begin_date=datetime.now(),
            nd_date=datetime.now() + timedelta(days=1),
        )

        existing_company = await db.fetch_one(select(Company).where(
            and_(
                Company.bin == str(Company_request.bin),  # Привести к строке
                cast(Company.company_data_1, JSONB) == Company_request.company_data_1,
                cast(Company.company_data_2, JSONB) == Company_request.company_data_2
            )
        ))

        if not existing_company:
            await db.execute(
                Company.__table__.insert().values(
                    bin=Company_request.bin,
                    company_data_1=Company_request.company_data_1,
                    company_data_2=Company_request.company_data_2,
                    begin_date=Company_request.begin_date,
                    nd_date=Company_request.nd_date,
                )
            )

        context = {"request": request, "company_data_1": company_data_1, "company_data_2": company_data_2}
        return templates.TemplateResponse("stat_response.html", context)

    except httpx.HTTPStatusError as e:
        return {"error": f"Stat API returned an error: {e.response.status_code}"}
    except Exception as e:
        return {"error": str(e)}
