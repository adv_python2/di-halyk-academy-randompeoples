from fastapi import FastAPI
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy import create_engine, MetaData
from app.db import database, DATABASE_URL, engine, metadata
from fastapi.templating import Jinja2Templates
from .models import Base
import asyncio
from app.models import User, ProfileChangeRequest

metadata = MetaData()


# engine = create_engine(DATABASE_URL)
# Base.metadata.create_all(bind=engine)


async def create_tables():
    async with database.transaction():
        await database.execute(
            """
            CREATE TABLE IF NOT EXISTS users (
                id SERIAL PRIMARY KEY,
                iin VARCHAR(255),
                name VARCHAR(255),
                sur_name VARCHAR(255),
                phone_number VARCHAR(20),
                hashed_password VARCHAR(100),
                gender VARCHAR(10),
                about_me TEXT,
                education VARCHAR(255),
                birthdate DATE,
                is_manager BOOLEAN DEFAULT FALSE  
            )
            """
        )

        await database.execute("CREATE UNIQUE INDEX IF NOT EXISTS idx_iin ON users (iin);")
        await database.execute("CREATE UNIQUE INDEX IF NOT EXISTS idx_phone_number ON users (phone_number);")
        await database.execute(
            """
            CREATE TABLE IF NOT EXISTS profile_change_requests (
                id SERIAL PRIMARY KEY,
                user_id INTEGER,
                field_to_change VARCHAR(255),
                new_value TEXT,
                status VARCHAR(20) DEFAULT 'pending'
            )
            """
        )
        await database.execute(
            """
            CREATE TABLE IF NOT EXISTS companies (
                id SERIAL PRIMARY KEY,
                bin VARCHAR(255),
                company_data_1 JSON,
                company_data_2 JSON,
                begin_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
                nd_date TIMESTAMP WITH TIME ZONE DEFAULT now() + interval '1 day'
            )
            """
        )
