from sqlalchemy import Column, Integer, String, Date, JSON, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Boolean
from app.db import metadata
from pydantic import BaseModel
from sqlalchemy import Column, Integer, String, ForeignKey, Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql import text
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.sql import func
Base = declarative_base(metadata=metadata)


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    iin = Column(String, unique=True, index=True)
    name = Column(String(255))
    sur_name = Column(String(255))
    phone_number = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    gender = Column(String, nullable=True)
    about_me = Column(String, nullable=True)
    education = Column(String, nullable=True)
    birthdate = Column(Date, nullable=True)
    is_manager = Column(Boolean, default=False)
    profile_change_requests = relationship("ProfileChangeRequest",
                                           back_populates="user")  


class ProfileChangeRequest(Base):
    __tablename__ = "profile_change_requests"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    # JsonbField
    old_data = Column(JSON, nullable=True)
    new_data = Column(JSON, nullable=True)
    status = Column(
        ENUM("pending", "approved", "rejected", name="profile_change_status"),
        server_default=text("'pending'"),
        nullable=False,
    )
    user = relationship("User", back_populates="profile_change_requests")

from sqlalchemy import Interval
from sqlalchemy import cast


class Company(Base):
    __tablename__ = "companies"

    id = Column(Integer, primary_key=True, index=True)
    bin = Column(String, unique=True, index=True)
    company_data_1 = Column(JSON, nullable=True)
    company_data_2 = Column(JSON, nullable=True)
    begin_date = Column(DateTime(timezone=True), server_default=func.now())
    nd_date = Column(DateTime(timezone=True), server_default=func.now() + cast(func.text("1 day"), Interval))