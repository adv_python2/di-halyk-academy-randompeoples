import httpx
from fastapi import APIRouter
import json

STAT_API_BASE_URL_1 = "https://old.stat.gov.kz/api"
STAT_API_BASE_URL_2 = "https://pk.adata.kz/api"

router = APIRouter()

async def get_company_data_by_bin_1(bin_number: int) -> dict:
    url1 = f"https://old.stat.gov.kz/api/juridical/counter/api/?bin={bin_number}&lang=ru"
    async with httpx.AsyncClient() as client:
        response = await client.get(url1)
        try:
            data = response.json()
            return data
        except json.JSONDecodeError:
            print("Недопустимый ответ JSON от API Company Data 1")
            return {}
        
        
async def get_company_data_by_bin_2(bin_number: int) -> dict:
    url2 = "https://gr5.gosreestr.kz/p/ru/api/v1/gr-objects"
    
    async with httpx.AsyncClient() as client:
        response = await client.get(url2)
        if response.status_code == 200:
            data2 = response.json()
            # print("API Response:", data2)  # Отладочный вывод

            objects = data2.get("Objects", [])
            
            for obj in objects:
                if obj.get("flBin") == str(bin_number): 
                    return {
                        "exists": True,
                        "Name": obj.get("flNameRu", ""),
                        "Opf": obj.get("flOpf", ""),
                        "Status": obj.get("flStatus", ""),
                        "StateInvolvement": obj.get("flStateInvolvement", ""),
                        "KfsL0": obj.get("flKfsL0", ""),
                        "KfsL1": obj.get("flKfsL1", ""),
                        "KfsL2": obj.get("flKfsL2", ""),
                        "OwnerBin": obj.get("flOwnerBin", ""),
                        "OguBin": obj.get("flOguBin", ""),
                    }

        return {"exists": False}


async def get_company_data_by_params(params: dict) -> str:
    url = f"{STAT_API_BASE_URL_1}/sbr/request/?api"
    async with httpx.AsyncClient() as client:
        response = await client.post(url, json=params)
        return response.json()["obj"]

async def check_request_status(request_number: str, lang: str) -> dict:
    url = f"{STAT_API_BASE_URL_1}/sbr/requestResult/{request_number}/{lang}"
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        return response.json()

async def download_request_data(request_guid: str) -> bytes:
    url = f"{STAT_API_BASE_URL_1}/sbr/download?bucket=SBR_UREQUEST&guid={request_guid}"
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        return response.content



async def get_company_data_by_params_2(params: dict) -> str:
    url = f"{STAT_API_BASE_URL_2}/sbr/request/?api"
    async with httpx.AsyncClient() as client:
        response = await client.post(url, json=params)
        return response.json()["obj"]

async def check_request_status_2(request_number: str, lang: str) -> dict:
    url = f"{STAT_API_BASE_URL_2}/sbr/requestResult/{request_number}/{lang}"
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        return response.json()

async def download_request_data_2(request_guid: str) -> bytes:
    url = f"{STAT_API_BASE_URL_2}/sbr/download?bucket=SBR_UREQUEST&guid={request_guid}"
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        return response.content