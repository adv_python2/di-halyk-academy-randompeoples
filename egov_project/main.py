from fastapi import FastAPI
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy import create_engine, MetaData
from app.api import logged, login, register, request, profile, manager_request
from app.db import database, DATABASE_URL, engine, metadata
from fastapi.templating import Jinja2Templates
from app.stat_integration.stat_api import router as stat_integration_router

app = FastAPI()

metadata.create_all(bind=engine)

templates = Jinja2Templates(directory="templates")


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

app.include_router(login.router)
app.include_router(register.router)
app.include_router(logged.router)
app.include_router(request.router)
app.include_router(profile.router)
app.include_router(manager_request.manager_router)
app.include_router(stat_integration_router, prefix="/api/stat") 
