## Name
Egov model

## Description
This project is mini version of the e-Government system of Kazakhstan. In this project users can register, log-in, submit an application for a data change and get personal information bu IIN.

## Detailed description
Scheme of the application is availible here:
https://excalidraw.com/%20t#json=7xATGKmSrhzB0Rmo4xgCo,VhXrVVHJ4CY-CiJjF38YRg

## Team members 
21B030459 - Nurbolat Amirbay is responsible for mostly dockers.
20B030452 - Dinara Mairambayeva is responsible for project management. 
20B030735 - Nurkhat Bekmaganbet is responsible for integrating different APIs
20B030479 - Alisher Akim is responsible for Kafka integration


## Installation
1. Download the project code from gitlab by the following link: https://gitlab.com/adv_python2/di-halyk-academy-randompeoples

2. Install project dependencies on the terminal:    
py -m pip install -r requirements.txt
    
3. Build and run the Docker containers:
bash:
    docker compose up --build

4. If you have Make installed:
bash
    make app-start
    make drop-app
    make logs
    
5. Visit [http://localhost:8000/login]

## Contributing
If you have any issues or bugs, please contact us with email: n_amirbai@kbtu.kz

## Interesting features 
Request change information about user and other

## Project status
in process of development  